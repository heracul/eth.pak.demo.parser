package eth.pak.demo.parser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.utils.CodeGenerationUtils;
import com.github.javaparser.utils.SourceRoot;

import java.io.IOException;

public class ParserMain {
    private static final String PROJECT_PATH = "C:\\projects\\ethan_projects\\eth.pak.demo.batch";
    private static final String JAVA_FILE_EXT = ".java";

    public static void main(String[] args) throws IOException {
        SourceRoot sourceRoot = new SourceRoot(CodeGenerationUtils.fileInPackageAbsolutePath(PROJECT_PATH, "src/main/java", ""));
        sourceRoot.parse("eth.pak.demo", (localPath, absolutePath, result) -> {
            result.getResult()
                    .map(CompilationUnit::getTypes)
                    .ifPresent(typeDeclarations -> typeDeclarations.forEach(typeDeclaration -> {
//                        typeDeclaration.getMethods().forEach(methodDeclaration -> {
//                            System.out.println("Method name : "+methodDeclaration.getName());
//                            methodDeclaration.getAnnotationByName("IAMFJob")
//                                        .map(annotationExpr -> {
//                                            if (annotationExpr.getClass().equals(NormalAnnotationExpr.class)) {
//                                                NormalAnnotationExpr annot = (NormalAnnotationExpr) annotationExpr;
//                                                return annot.getPairs();
//                                            } else {
//                                                return new NodeList<MemberValuePair>();
//                                            }
//                                        })
//                                        .ifPresent(memberValuePairs ->
//                                                        memberValuePairs.forEach(memberValuePair ->
//                                                                System.out.println(memberValuePair.getName() +":"+ memberValuePair.getValue())
//                                                        )
//                                        );
//                        });
                        typeDeclaration.getAnnotationByName("IAMFJob")
                                        .map(annotationExpr -> {
                                            if (annotationExpr.getClass().equals(NormalAnnotationExpr.class)) {
                                                NormalAnnotationExpr annot = (NormalAnnotationExpr) annotationExpr;
                                                return annot.getPairs();
                                            } else {
                                                return new NodeList<MemberValuePair>();
                                            }
                                        })
                                        .ifPresent(memberValuePairs ->
                                                        memberValuePairs.forEach(memberValuePair ->
                                                                System.out.println(memberValuePair.getName() +":"+ memberValuePair.getValue())
                                                        )
                                        );

                    }));
            return SourceRoot.Callback.Result.DONT_SAVE;
        });
    }
}

